#!/usr/bin/python
# coding: UTF-8

import discord
import asyncio
import re
import connection_controller as cc

command_throttled   = {}

client = discord.Client()


debug_join_channel   = '[*] Connecting to %s on %s.'
error_join_channel   = '[!] ERROR: Unable to connect to %s on %s. Permissions = %s.'

regex_illum = re.compile(r'illuminati([\s\W]+)?confirmed', re.I)



async def command_throttling(server):
    if server not in command_throttled.keys():
        command_throttled[server] = 1

        asyncio.sleep(1)

        command_throttled.pop(server)


@client.event
async def on_ready():
    for server in client.servers:
        print('[*] Connected to %s.' % (server))


@client.event
async def on_server_join(server):
    print('[*] Connected to %s.' % (server))


@client.event
async def on_message(message):
    if message.server not in command_throttled.keys():
        await command_throttling(message.server)
        server  = message.server
        channel = message.author.voice_channel

        #if message.content.startswith('!join'):
        #    await cc.connect_to_channel(client, server, channel, True)

        if message.content.startswith('!pause'):
            if server in cc.all_channel_connections.keys():
                await cc.all_channel_connections[server].stop_media_player()

        if message.content.startswith('!stop'):
            if server in cc.all_channel_connections.keys():
                await cc.all_channel_connections[server].stop_media_player(True)

        if message.content.startswith('!play'):
            vid_regex = re.compile(r'(youtube.com/watch\?v=|youtu.be/)([\w-]+)')
            video     = vid_regex.search(message.content)

            if video:
                url_suffix = str(video.group(2)) # Sanitize input
                url = 'https://www.youtube.com/watch?v=%s' % (url_suffix)

                connection = await cc.connect_to_channel(client, server, channel)
                await connection.play_youtube_video(url)

            elif server in cc.all_channel_connections.keys():
                await cc.all_channel_connections[server].play_youtube_video()

        if message.content.startswith('!debug'):
            print(cc.all_channel_connections[server].media)
            print(cc.all_channel_connections[server].is_playing)
            print(cc.all_channel_connections[server].media.is_playing())








        if regex_illum.findall(message.content):
            url     = 'https://www.youtube.com/watch?v=NtrJxqj_wGY?t=24s'

            await cc.connect_to_channel(client, server, channel)
            await cc.all_channel_connections[server].play_youtube_video(url)


        if message.content.startswith('DICK'):
            await client.send_message(message.channel, '**IS**')
            await client.send_message(message.channel, '**SUPREME**')

with open('token', 'r') as token_file:
    token = token_file.readlines()[0].replace('\n', '')
    token_file.close()

    client.run(token)
