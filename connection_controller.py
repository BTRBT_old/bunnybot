#!/usr/bin/python
# coding: UTF-8

import discord
import asyncio

# Remember to fix the sticky_join update

all_channel_connections = {}

class channel_connection():
    def __init__ (self, server, channel, voice, sticky_join = False):
        self.server      = server
        self.channel     = channel
        self.voice       = voice
        self.sticky_join = sticky_join
        self.media       = 'None'
        self.is_playing  = False

    async def disconnect_from_channel(self, command = False):
        if command or self.sticky_join == False:
            await self.voice.disconnect()
            all_channel_connections.pop(self.server, None)

    async def play_youtube_video(self, url = 'None'):
        if self.media == 'None':
            print('Loading video: %s' %(url))
            self.media = 'Loading'
            self.media = await self.voice.create_ytdl_player(url)

            self.media.start()
            self.is_playing = True

            while self.media != 'None':
                if self.media.is_done() == True:
                    self.media = 'None'
                    await self.disconnect_from_channel()

        elif isinstance(self.media, str) == False:
            print('Resuming play')
            if self.is_playing == False:
                self.media.resume()
                self.is_playing = True

    async def stop_media_player(self, full_stop = False):
        if isinstance(self.media, str) == False:
            if full_stop and self.media.is_playing():
                print('Stopping media player')
                self.media.stop()
                self.media = 'None'

                await self.disconnect_from_channel()

            elif self.media.is_playing():
                print('Pausing media player')
                self.media.pause()
                self.is_playing == False


async def get_connection(server):
    if server in all_channel_connections.keys():
        return all_channel_connections[server]

    else:
        return False


async def connect_to_channel(client, server, channel, sticky_join = False):
    bunnybot   = discord.utils.get(server.members, id = client.user.id)
    connection = await get_connection(server)

    if channel != 'None' and bunnybot.permissions_in(channel).connect:
        if connection:
            if connection.is_playing == False and connection.channel != channel:
                print('Moving to new channel')
                connection.voice.move_to(channel)

                connection.channel    = channel
                connection.stick_join = sticky_join

        else:
            print('Connecting to channel')
            voice = await client.join_voice_channel(channel)
            info  = channel_connection(server, channel, voice, sticky_join)

            all_channel_connections[server] = info

        return all_channel_connections[server]

    else:
        permission = bunnybot.permissions_in(channel).connect

        print('CONNECTION ERROR | CHANNEL = %s | PERMISSION = %s') % (channel, permission)
